import nose
from acp_times import open_time, close_time


def test_start_open():
    start_iso = "2020-05-15T01:00:00"
    km = 0
    brevet_dist = 200
    result = open_time(km, brevet_dist, start_iso)
    assert result == '2020-05-15T01:00:00+00:00' or result == '2020-05-15T01:00:00'

def test_start_close():
    start_iso = "2020-05-15T01:00:00"
    km = 0
    brevet_dist = 200
    result = close_time(km, brevet_dist, start_iso)
    assert result == '2020-05-15T02:00:00+00:00'

def test_intermediate_open(): 
    start_iso = "2020-05-15T01:00:00"
    dist1 = 200
    dist2 = 777
    dist3 = 999.4
    result1 = open_time(dist1, 1000, start_iso)
    result2 = open_time(dist2, 1000, start_iso)
    result3 = open_time(dist3, 1000, start_iso)
    assert result1 == '2020-05-15T06:53:00+00:00'
    assert result2 == '2020-05-16T02:07:00+00:00'
    assert result3 == '2020-05-16T10:03:00+00:00'


def test_intermediate_close(): 
    start_iso = "2020-05-15T01:00:00"
    dist1 = 200
    dist2 = 777
    dist3 = 999.4
    result1 = close_time(dist1, 1000, start_iso)
    result2 = close_time(dist2, 1000, start_iso)
    result3 = close_time(dist3, 1000, start_iso)
    assert result1 == '2020-05-15T14:20:00+00:00'
    assert result2 == '2020-05-17T08:29:00+00:00'
    assert result3 == '2020-05-18T03:55:00+00:00'


def test_end_open():
    start_iso = "2020-05-15T01:00:00"
    dist1 = 1000
    dist2 = 1010
    dist3 = 1100.5
    result1 = open_time(dist1, 1000, start_iso)
    result2 = open_time(dist2, 1000, start_iso)
    result3 = open_time(dist3, 1000, start_iso)


def test_end_close():
    start_iso = "2020-05-15T01:00:00"
    dist1 = 1000
    dist2 = 1010
    dist3 = 1100.5
    result1 = close_time(dist1, 1000, start_iso)
    result2 = close_time(dist2, 1000, start_iso)
    result3 = close_time(dist3, 1000, start_iso)
    assert result1 == '2020-05-18T04:00:00+00:00'    
    assert result2 == '2020-05-18T04:00:00+00:00'
    assert result3 == '2020-05-18T04:00:00+00:00'
